package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    EditText name,region,loc,climat,area;
    Intent intent;
    //Wine wine;
    Button save;
    WineDbHelper mydb;
    Wine wine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mydb=new WineDbHelper(getApplicationContext());
        setContentView(R.layout.activity_wine);
        name=findViewById(R.id.wineName);
        region=findViewById(R.id.editWineRegion);
        loc=findViewById(R.id.editLoc);
        climat=findViewById(R.id.editClimate);
        area=findViewById(R.id.editPlantedArea);
        save=findViewById(R.id.save);
        intent=getIntent();


        if(intent.hasExtra("wine")) {
            wine = intent.getParcelableExtra("wine");
            name.setText(wine.getTitle());
            loc.setText(wine.getLocalization());
            region.setText(wine.getRegion());
            climat.setText(wine.getClimate());
            area.setText(wine.getPlantedArea());
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    wine.setId(wine.getId());
                    wine.setTitle(name.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setLocalization(loc.getText().toString());
                    wine.setClimate(climat.getText().toString());
                    wine.setPlantedArea(area.getText().toString());
                    if(wine.getTitle().equals("") || wine.getRegion().equals("") ){
                        AlertDialog.Builder builder=new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("l'un des champ est vide");
                        builder.setCancelable(true);
                        builder.show();
                    }else {
                        int res=mydb.updateWine(wine);
                        if(res>0){
                        Intent intent1=new Intent(WineActivity.this,MainActivity.class);
                        startActivity(intent1);
                            finish();}
                        else {Toast.makeText(WineActivity.this,"le vin existe déja "+wine.getTitle().toString(),Toast.LENGTH_LONG).show(); }
                    }
                }
            });
        }else {
            name.setText("");
            save.setOnClickListener(new View.OnClickListener() {
                //ajout d'un wine
                @Override
                public void onClick(View v) {

                    Wine w=new Wine();
                    w.setTitle(name.getText().toString());
                    w.setRegion(region.getText().toString());
                    w.setLocalization(loc.getText().toString());
                    w.setClimate(climat.getText().toString());
                    w.setPlantedArea(area.getText().toString());
                    if(w.getTitle().equals("") || w.getRegion().equals("") ){
                        AlertDialog.Builder builder=new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("l'un des champ est vide");
                        builder.setCancelable(true);
                        builder.show();
                    }else {
                        Boolean res=mydb.addWine(w);
                        if(res==true){
                        Intent intent1=new Intent(WineActivity.this,MainActivity.class);
                        startActivity(intent1);
                            finish();}
                        else{ Toast.makeText(WineActivity.this,"le vin existe déja "+w.getTitle().toString(),Toast.LENGTH_LONG).show();}
                    }

                }
            });


        }


    }
}
