package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    WineDbHelper mywineDb;
    FloatingActionButton add;
    ListView wines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add=findViewById(R.id.add);
        wines = (ListView) findViewById(R.id.lvwine);
        mywineDb=new WineDbHelper(getApplicationContext());
        if (mywineDb.fetchAllWines().getCount() < 1) {
            mywineDb.populate();
        }
        refresh();

        wines.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                // Création de l'intent
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                // Ajout dans l'intent de l'objet wine
                intent.putExtra("wine", mywineDb.cursorToWine(item));
                // Envoyer l'intent
                startActivity(intent);
               // finish();

            }
        });
        // Specify that my listview has a context menu attached
        registerForContextMenu(wines);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentadd = new Intent(MainActivity.this,WineActivity.class);
                startActivity(intentadd);
               // finish();
            }
        });

    }
    public void refresh(){
        Cursor result=mywineDb.fetchAllWines();
        result.moveToFirst();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, result, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[]{android.R.id.text1, android.R.id.text2}, 0);
        wines.setAdapter(adapter);
    }
    //creation du menu attaché a la listview
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        menu.add(0, v.getId(), 0, "Supprimer");
    }


    //la fonction executé apres la selection de l'item supprimer
    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Supprimer"){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor selectedwine = (Cursor)wines.getItemAtPosition(info.position);
            mywineDb.deleteWine(selectedwine);
            refresh();


        }
        return true;
    }

}
