package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME
            + " TEXT UNIQUE, " + COLUMN_WINE_REGION + " TEXT, " + COLUMN_LOC
            + " TEXT, " +COLUMN_CLIMATE + " TEXT, "+COLUMN_PLANTED_AREA + " TEXT )";
    public static final String METIER="DROP TABLE IF EXISTS "+TABLE_NAME+";";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        //  the CREATE TABLE ... command
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(METIER);
        onCreate(db);
    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COLUMN_NAME,wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION,wine.getRegion());
        contentValues.put(COLUMN_LOC,wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE,wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        // Inserting Row
        long rowID=0;
        rowID=db.insert(TABLE_NAME,null,contentValues);

        // Closing database connection
        db.close();

        if(rowID>0) return true;
        else return false;

    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = 0;
        ContentValues newValues=new ContentValues();
        newValues.put(COLUMN_NAME,wine.getTitle());
        newValues.put(COLUMN_WINE_REGION,wine.getRegion());
        newValues.put(COLUMN_LOC,wine.getLocalization());
        newValues.put(COLUMN_CLIMATE,wine.getClimate());
        newValues.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        // updating row
        res= db.update(TABLE_NAME, newValues, _ID + " = ?",
                new String[] { String.valueOf(wine.getId()) });
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM cellar" ;
        Cursor cursor;


        // selecte all
        cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        Wine wine=cursorToWine(cursor);

        // call
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[] { String.valueOf(wine.getId()) });
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        String id=cursor.getString(0);
        Wine wine=new Wine();
        wine.setId((Integer.parseInt(id)));
        wine.setTitle(cursor.getString(1));
        wine.setRegion(cursor.getString(2));
        wine.setLocalization(cursor.getString(3));
        wine.setClimate(cursor.getString(4));
        wine.setPlantedArea(cursor.getString(5));
        return wine;
    }

}

